# 全都是写

合抱之木，生于毫末；九层之台，起于累土；千里之行，始于足下。

:sunny:

## 2018

|                         名称                         |                                        简介                                        |       时间       |
| :--------------------------------------------------: | :--------------------------------------------------------------------------------: | :--------------: |
|  [《如何阅读一本书》](/docs/《如何阅读一本书》.md)   |                     教你各种姿势阅读好一个本书，积累宝贵的财富                     | 2018.1 - 2018.12 |
| [《程序员修炼之道——从小工到专家》](/docs/issue-1.md) |                               讲述编程中的技巧与哲学                               | 2018.1 - 2018.12 |
|       [【TED】滚蛋吧小情绪](/docs/issue-2.md)        |                               不要忽视自己的心理健康                               | 2018.1 - 2018.12 |
|           [《浮世画家》](/docs/issue-3.md)           |         小野增二固守着内心过时的价值标准，但是一些事让他开始审视自己的过去         | 2018.1 - 2018.12 |
|           [《睡眠革命》](/docs/issue-4.md)           |        睡得多≠睡得好，高质量的睡眠是让生活效率更高、让精神状态更好的关键。         | 2018.1 - 2018.12 | **** |
|    [【一席】何志森：城市跟踪者](/docs/issue-5.md)    |         小野增二固守着内心过时的价值标准，但是一些事让他开始审视自己的过去         | 2018.1 - 2018.12 |
|          [《全球航空业》](/docs/issue-6.md)          | 全球航空业已经发展为一个由互相关联的技术、经济和组织部件构成的成熟、庞大、复杂体系 | 2018.1 - 2018.12 |
|    [凭什么你认为自己不会画画？](/docs/issue-8.md)    | 勇敢迈出第一步，世界充满无限可能！| 2018.1 - 2018.12 |
|        [我是如何战胜怯场的](/docs/issue-9.md)        | 勇敢的面对问题吧！办法总比困难多！ | 2018.1 - 2018.12 |


## 2019

|                         名称                         |                                        简介                                        |       时间       |
| :--------------------------------------------------: | :--------------------------------------------------------------------------------: | :--------------: |
|               [结缘书](/docs/issue-7.md)             | 与佛结缘，何乐不为                                                                     | 2019.4 - 2019.4 |
|               [游戏学](/docs/issue-11.md)             | 研究游戏，时不我待。                                                                     | 2019.5 - 2019.7 |
|               [人工智能](/docs/issue-10.md)             |  拥抱人工智能                                                          | 2019.5 - 2019.7 |

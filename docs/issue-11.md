# 游戏学

`作者` *北京大学互联网发展研究中心*

`版本` *2019 年 3 月第 1 版*

## 分类

`游戏` `专业` `论说类`

## 一句话总结

本书通过产业发展、文化传播、学科发展的视角来说明建立游戏学是很有必要的。

## 框架

## 目录

- ### 第一章 从游戏到游戏学

  游戏研究能否成为一门学科？
  - **第一节 游戏的概念**
      什么是游戏？游戏是个很广的词语，必要元素：规律、障碍、挑战、乐趣
  - **第二节 游戏的基本性质**
      规则性、目标导向性、娱乐性、交互性、二象性
  - **第三节 从游戏到游戏学**
      游戏应用发展巨大，而游戏研究寥寥无几

- ### 第二章 游戏的历史与发展

    游戏的历史和人类的历史相伴相生。
  - **第一节 游戏的诞生与早期发展**

     有人就有游戏。甚至人类出现之前的动物世界中就存在。
  - **第二节 近现代以前的游戏**
  - **第三节 近现代的游戏**
  - **第四节 电子游戏的产生与发展**
  - **第五节 2012-2016年智能手机游戏——中国手游的革新贡献**

- ### 第三章 游戏的社会功能

  - **第一节 游戏的娱乐功能**

游戏和时间的平衡
  - **第二节 游戏的教育功能**

游戏的最高层次和最有意义的价值应该是游戏精神。
  - **第三节 游戏的跨文化传播**

游戏是软媒介，也是意识形态的载体。
  - **第四节 游戏的文化功能**

如何让已经存在的好游戏，在我们生活的方方面面发挥更显著的价值。
  - **第五节 功能游戏：游戏社会功能的集中体现**

以解决现实社会和行业问题为主要功能诉求的游戏品类。

- ### 第四章 游戏文化
  - **第一节 游戏世界的文化狂欢**

1. 魔兽世界
2. 我的世界
3. 仙侠文化
4. 王者荣耀
5. 阴阳师
  - **第二节 非游戏世界的文化延伸**

1. cosplay
2. 文化产品
3. 游戏嘉年华

- ### 第五章 游戏产业
  - **第一节 国内游戏产业现状**

从产业规模和移动游戏分析，国内为世界最大。
  - **第二节 国内游戏产业主体**

我：技术的发展让开发者能和用户零距离接触。
1. 游戏研发商
2. 游戏运营支持商
3. 游戏运营商
4. 渠道业
5. 电子竞技&游戏直播

  - **第三节 中国游戏产业竞争力**

1. 成功的关键因素
2. SWOT分析

  - **第四节 中美游戏产业发展历程差异**

1. 中美游戏产业四大历史差异
2. 1947-2000 年前网游时代的历史路径
3. 2001-2012 年网游时代的历史路径
4. 2013-2018 年手游时代的历史路径
5. 未来趋势：专业化电子竞技扩展边界，技术进步与改革改变游戏形态
  - **第五节 中美游戏产业外部环境差异**

1. 政策-版权保护，技术发展
2. 经济-玩家花钱
3. 社会-美国行业自律
4. 技术发展
5. 人才环境

  - **第六节 中美游戏产业现状差异**

1. 游戏产业竞争格局对比
2. 游戏产业生态对比
3. 产业市场对比需求
  - **第七节 游戏产业的地位与意义**

对内有利于增强我国文化安全，对外有助讲好中国故事。

- ### 第六章 游戏玩家

消费者的需求和体验都是重中之重
  - **第一节 游戏用户心理与行为**
1. 用户下载的决策
2. 游戏心理诉求及行为分析
    | 动作 | 社交 | 掌控 | 成就 | 沉浸 | 创造 |
    | ---- | ---- | ---- | ---- | ---- | ---- |
    | 破坏 | 竞争 | 挑战 | 完成 | 幻想 | 设计 |
    | 刺激 | 社群 | 战略 | 力量 | 故事 | 发现 |

3. 用户衍生需求行为
4. 游戏的其他价值
    - 审美体验
    - 获得益智
    - 运动建生
    - 学习知识
    - 促进亲子关系

  - **第二节 游戏学中的用户研究方法**
1. 用户研究的主要方法
2. 研究的发展
3. 研究的特殊方法

- ### 第七章 游戏治理
  - **第一节 国外防游戏沉迷对策**
  - **第二节 游戏分级**
  - **第三节 国际游戏治理与青少年保护**
  - **第四节 我国未成年人网络保护现状及问题**
  - **第五节 未成年人网络保护的国际经验和启示**
- ### 第八章 游戏的发展与未来
  - **第一节 游戏的智能化革新**
  - **第二节 AR/VR技术：沉浸式的游戏体验**
  - **第三节 游戏社会化，功能游戏崛起**
  - **第四节 电竞成为游戏行业发展重要助力点**

## 后记

- 游戏产业的发展是前途光明的，其蕴含的游戏精神与大众所追求的   自由、竞争等精神不谋而合，但目前的欧系发展存在问题，必须   对它进行研究。
- 明确研究对象，在游戏中究竟要研究游戏还是研究游戏背后的人，或是研究游戏的设计者，三个不同层次
- 产业发展、文化传播、科学发展都需要推动游戏学的建立。未来需要学术平台、学术资源、学术共同体以及学术经典的建立。

## 写在最后

写笔记的时候，百度了一些相关资料，虽然这本书评价并不高，但是号称国内第一本讲游戏学的书，看完觉得还是名副其实。书中写的很全，眼光也放的长远，介绍了很多跟游戏相关的知识，而且对我来说很新奇，关于游戏前行的方向和历史的发展,引人深思。收获很多，也正如书中所提，游戏不光光是有关电子游戏，应该从几千年人类历史的游戏活动中寻得根基。

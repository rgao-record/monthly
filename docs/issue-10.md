# 人工智能

`作者` *周晓垣*

`出版时间` *2018 年 5 月*

`版本` *2018 年 5 月第 1 版*

## 分类

`计算机` `人工智能` `科普`

## 前言

前言摘要：

> * 人工智能上升到国家战略层面
> * 机遇和挑战并存。狄更斯——这是最好的时代也是最坏的时代。

## 内容简介

人工智能已经来了，它就在我们身边，几乎无处不在。 人工智能技术正在彻底改变人类的认知。史无前例的自动驾驶正在重构我们头脑中的出行地图和人类生活图景，人脸识别、智能购物、人工智能、大数据、无人驾驶……从远在千里之外的航天科技，到近在身边的智能家居，未来的生活到底会有多智能？ 当人工智能时代成为必然，个人应该做些什么，才能避免被AI取代？企业应该如何升级，才能在新的商业变局到来前抓住先机？《人工智能：开启颠覆性智能时代》一书告诉我们： 无需担忧和惧怕人工智能时代的到来，我们所要做的，应当是尽早认清AI与人类的关系，了解变革的规律，更好地拥抱新时代的到来。

## 正文到此结束

本书主要是科普的性质讲解人工智能

1. 人工智能的进化史
2. AI挑战人类
3. 带给我们的影响
4. 怎么面对人工智能（个人、企业）
5. 大佬们对 AI 的观点
6. 智慧金融（安全最关键）

这本书把金融单独用了一章节来写，作者眼中觉得这是人类的游戏，AI 介入后肯定会稳赚不赔吧？可我还是觉得农业才是最重要的，毕竟民以食为天嘛···书中最后附带了国家新一代人工智能发展规划讲述了2017年到2030年人工智能三步战略的规划发展。
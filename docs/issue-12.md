# 人工智能

`又名` *THE GLOBAL AIRLINE INDUSTRY*

`作者` *（美国）彼得·贝罗巴巴（Peter P.Belobaba） （美国）阿梅迪奥·奥多尼（Amedeo R.Odoni） （美国）辛西娅·巴恩哈特（Cynthia Barnhart）*

`译者` *解开颜、 志军*

`出版时间` *2011 年 7 月*

`版本` *2018 年 7 月第 1 版*

## 分类

`论述` `航空` `人文` `专业`

## 前言

## 框架

### 1 引言与概述

## 正文到此结束

1. 自动驾驶为什么不用到一些本身安全性就很高的交通工具上，或者低速？